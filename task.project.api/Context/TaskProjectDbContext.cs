using Microsoft.EntityFrameworkCore;
using task.project.api.Domain.Entities;

namespace task.project.api.Context
{
    public class TaskProjectDbContext : DbContext
    {
        public TaskProjectDbContext(DbContextOptions<TaskProjectDbContext> options) : base(options)
        {

        }

        public DbSet<TaskEntity> Tasks { get; set; }
        public DbSet<CategoryEntity> Categories { get; set; }
        public DbSet<RoleEntity> Roles { get; set; }
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<ScheduleEntity> Schedules { get; set; }
        public DbSet<SchedulerEntity> Schedulers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TaskEntity>(x => x.HasOne(x => x.Category)
                        .WithMany(x => x.Tasks)
                        .HasForeignKey(x => x.CategoryId));

            modelBuilder.Entity<CategoryEntity>(x => x.HasMany(x => x.Tasks)
                        .WithOne(x => x.Category)
                        .HasForeignKey(x => x.CategoryId)
                        .IsRequired());

            modelBuilder.Entity<UserEntity>(x => x.HasOne(x => x.Role)
                        .WithMany(x => x.Users)
                        .HasForeignKey(x => x.RoleId));
            modelBuilder.Entity<RoleEntity>(x => x.HasMany(x => x.Users)
                        .WithOne(x => x.Role)
                        .HasForeignKey(x => x.RoleId)
                        .IsRequired());

            modelBuilder.Entity<ScheduleEntity>(x => x.HasOne(x => x.Scheduler)
                        .WithMany(x => x.Schedules)
                        .HasForeignKey(x => x.SchedulerId));
            modelBuilder.Entity<SchedulerEntity>(x => x.HasMany(x => x.Schedules)
                        .WithOne(x => x.Scheduler)
                        .HasForeignKey(x => x.SchedulerId)
                        .IsRequired());
        }
    }
}