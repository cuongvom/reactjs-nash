using System.Collections.Generic;
using System.Threading.Tasks;
using task.project.api.Domain.Dto;

namespace task.project.api.Service
{
    public interface ITaskService
    {
        public Task<List<TaskDto>> GetTasks();
        public Task<TaskDto> TaskSpecific(int taskId);
        public Task<TaskDto> Create(TaskDto task);
        public Task Delete(int taskId);
        public Task<TaskDto> Update(TaskDto task);
    }
}