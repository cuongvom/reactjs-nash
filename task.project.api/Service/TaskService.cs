using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using task.project.api.Context;
using task.project.api.Domain.Dto;
using task.project.api.Domain.Entities;

namespace task.project.api.Service
{
    public class TaskService : ITaskService
    {
        private readonly TaskProjectDbContext _context;
        private readonly IMapper _mapper;
        public TaskService(TaskProjectDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<TaskDto> Create(TaskDto taskDto)
        {
            try
            {
                var task = new TaskEntity(){
                    Title = taskDto.Title,
                    Description = taskDto.Description,
                    CategoryId = taskDto.StatusTask
                };
                await _context.Tasks.AddAsync(task);
                _context.SaveChanges();
                return taskDto;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return new TaskDto();
            }
        }

        public async Task Delete(int taskId)
        {
            var task = await GetTaskAsync(taskId);
            _context.Tasks.Remove(task);
            _context.SaveChanges();
        }

        public async Task<List<TaskDto>> GetTasks()
        {
            var tasks = await _context.Tasks.ToListAsync();
            return _mapper.Map<List<TaskDto>>(tasks);
        }

        public async Task<TaskDto> TaskSpecific(int taskId)
        {
            var task = await GetTaskAsync(taskId);
            return _mapper.Map<TaskDto>(task);
        }

        public async Task<TaskDto> Update(TaskDto taskDto)
        {
            var task = await GetTaskAsync(taskDto.Id);
            task.CategoryId = taskDto.StatusTask;
            task.Description = taskDto.Description;
            task.Title = taskDto.Title;
            _context.Entry(task).State = EntityState.Modified;
            _context.SaveChanges();
            return _mapper.Map<TaskDto>(task);
        }

        private async Task<TaskEntity> GetTaskAsync(int taskId)
        {
            var task = await _context.Tasks.FindAsync(taskId);
            return task;
        }
    }
}