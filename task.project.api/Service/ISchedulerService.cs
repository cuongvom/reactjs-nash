using System.Collections.Generic;
using System.Threading.Tasks;
using task.project.api.Domain.Dto;

namespace task.project.api.Service
{
    public interface ISchedulerService
    {
        public Task<List<SchedulerDto>> GetAllAsync();
        public Task<SchedulerDto> AddSchedulerAsync(SchedulerDto schedulerDto);
        public Task<SchedulerDto> UpdateSchedulerAsync(SchedulerDto schedulerDto);
        public Task DeleteSchedulerAsync(int schedulerId);
    }
}