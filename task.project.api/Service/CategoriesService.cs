using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using task.project.api.Context;
using task.project.api.Domain.Dto;

namespace task.project.api.Service
{
    public class CategoryService : ICategoryService
    {
        private readonly TaskProjectDbContext _context;
        private readonly IMapper _mapper;
        public CategoryService(TaskProjectDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<List<CategoryDto>> GetCategoryAsync()
        {
            var categories = await _context.Categories.ToListAsync();
            return _mapper.Map<List<CategoryDto>>(categories);
        }
    }
}