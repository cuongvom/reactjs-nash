using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using task.project.api.Context;
using task.project.api.Domain.Entities;

namespace task.project.api.Seed
{
    public static class SeedData
    {
        public static async Task EnsureSeedDataAsync(string connectionString)
        {
            var services = new ServiceCollection();
            services.AddLogging();
            services.AddDbContext<TaskProjectDbContext>(options => options.UseSqlite(connectionString));
            using (var serviceProvider = services.BuildServiceProvider())
            {
                using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    var context = scope.ServiceProvider.GetService<TaskProjectDbContext>();
                    context.Database.Migrate();
                    await CreateCategoryAsync(context);
                    await CreateRoleAsync(context);
                    await CreateUserAsync(context);
                    await CreateTaskAsync(context);
                    await CreateSchedulerAsync(context);
                    await CreateScheduleAsync(context);
                    context.SaveChanges();
                }
            }
        }

        private static async Task CreateCategoryAsync(TaskProjectDbContext context)
        {
            var categories = await context.Categories.ToListAsync();
            if (categories.Count == 0)
            {
                var listCategory = new List<CategoryEntity>(){
                    new CategoryEntity(){
                        Name = "New"
                    },
                    new CategoryEntity(){
                        Name = "In Progress"
                    },
                    new CategoryEntity(){
                        Name = "Done"
                    }
                };
                context.Categories.AddRange(listCategory);
            }
        }
        private static async Task CreateRoleAsync(TaskProjectDbContext context)
        {
            var roles = await context.Roles.ToListAsync();
            if (roles.Count == 0)
            {
                var listRole = new List<RoleEntity>(){
                    new RoleEntity(){
                        Id = new Guid("e036960c-6e52-4018-a917-1aacde537e3f"),
                        RoleName = string.Format("admin").ToUpper()
                    },
                    new RoleEntity(){
                        Id = new Guid("55d3c773-0f12-4947-9d74-47e620bb53a7"),
                        RoleName = string.Format("member").ToUpper()
                    }
                };
                context.Roles.AddRange(listRole);
            }
        }
        private static async Task CreateUserAsync(TaskProjectDbContext context)
        {
            var users = await context.Users.ToListAsync();
            if (users.Count == 0)
            {
                var listUser = new List<UserEntity>(){
                    new UserEntity(){
                        Id = new Guid("4dce0d91-e845-4181-beef-03474fa53823"),
                        UserName = "admin",
                        Password = "Aa@123",
                        RoleId = new Guid("e036960c-6e52-4018-a917-1aacde537e3f")
                    }
                };
                context.Users.AddRange(listUser);
            }
        }

        private static async Task CreateTaskAsync(TaskProjectDbContext context)
        {
            var tasks = await context.Tasks.ToListAsync();
            if (tasks.Count == 0)
            {
                var listTask = new List<TaskEntity>(){
                    new TaskEntity(){
                        Title = "Task 1",
                        Description = "Work 1",
                        CategoryId = 1
                    },
                    new TaskEntity(){
                        Title = "Task 2",
                        Description = "Work 2",
                        CategoryId = 1
                    },
                    new TaskEntity(){
                        Title = "Task 3",
                        Description = "Work 3",
                        CategoryId = 1
                    },
                    new TaskEntity(){
                        Title = "Task 1",
                        Description = "Work 1",
                        CategoryId = 2
                    },
                    new TaskEntity(){
                        Title = "Task 1",
                        Description = "Work 1",
                        CategoryId = 3
                    },
                    new TaskEntity(){
                        Title = "Task 2",
                        Description = "Work 2",
                        CategoryId = 3
                    }
                };
                context.Tasks.AddRange(listTask);
            }
        }

        private static async Task CreateSchedulerAsync(TaskProjectDbContext context)
        {
            var schedulers = await context.Schedulers.ToListAsync();
            if (schedulers.Count == 0)
            {
                context.Schedulers.Add(new SchedulerEntity()
                {
                    Name = "Admin",
                    Position = "Admin"
                });
            }
        }

        private static async Task CreateScheduleAsync(TaskProjectDbContext context)
        {
            var schedules = await context.Schedules.ToListAsync();
            if (schedules.Count == 0)
            {
                context.Schedules.Add(new ScheduleEntity()
                {
                    Title = "Create Task",
                    Description = "Create Task",
                    Location = "E town",
                    StartDate = DateTime.Now.ToShortDateString(),
                    EndDate = DateTime.Now.AddDays(1).ToShortDateString(),
                    SchedulerId = 1,
                    Creator = "Admin"
                });
            }
        }
    }
}