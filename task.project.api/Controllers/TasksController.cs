using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using task.project.api.Domain.Dto;
using task.project.api.Service;

namespace task.project.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class TasksController : ControllerBase
    {

        private readonly ITaskService _service;
        public TasksController(ITaskService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<IActionResult> GetTasksAsync()
        {
            var result = await _service.GetTasks();
            return Ok(result);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSpecificAsync(int id)
        {
            var result = await _service.TaskSpecific(id);
            return Ok(result);
        }
        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] TaskDto taskDto)
        {
            var result = await _service.Create(taskDto);
            return Created("", result);
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            await _service.Delete(id);
            return Ok();
        }
        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] TaskDto taskDto)
        {
            var result = await _service.Update(taskDto);
            return Accepted(result);
        }
    }
}