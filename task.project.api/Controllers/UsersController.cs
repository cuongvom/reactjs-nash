using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using task.project.api.Domain.Dto;
using task.project.api.Service;

namespace task.project.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _service;
        public UsersController(IUserService service)
        {
            _service = service;
        }

        [HttpPost("login")]
        public async Task<IActionResult> LoginAsync([FromBody] AccountDto accountDto)
        {
            var result = await _service.GetTokenAsync(accountDto);
            return Ok(new {token = result});
        }
    }
}