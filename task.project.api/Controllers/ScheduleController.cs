using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using task.project.api.Domain.Dto;
using task.project.api.Service;

namespace task.project.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class ScheduleController : ControllerBase
    {
        private readonly IScheduleService _service;
        public ScheduleController(IScheduleService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var result = await _service.GetAllAsync();
            return Ok(result);
        }
        [HttpGet("{schedulerId}")]
        public async Task<IActionResult> GetAllBySchedulerIdAsync(int schedulerId)
        {
            var result = await _service.GetAllBySchedulerIdAsync(schedulerId);
            return Ok(result);
        }
        [HttpPost]
        public async Task<IActionResult> AddAsync([FromBody] ScheduleDto scheduleDto)
        {
            var result = await _service.AddSchedule(scheduleDto);
            return Created("", result);
        }
        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] ScheduleDto scheduleDto)
        {
            var result = await _service.UpdateSchedule(scheduleDto);
            return Accepted(result);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            await _service.DeleteSchedule(id);
            return Ok();
        }
    }
}