using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace task.project.api.Domain.Entities
{
    public class CategoryEntity : BaseEntity<int>
    {
        public string Name { get; set; }
        public IList<TaskEntity> Tasks { get; set; }
    }
}