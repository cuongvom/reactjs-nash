using System;

namespace task.project.api.Domain.Entities
{
    public class ScheduleEntity : BaseEntity<int>
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public string Creator { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int SchedulerId { get; set; }
        public SchedulerEntity Scheduler { get; set; }
    }
}