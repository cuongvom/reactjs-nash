using System.Collections.Generic;

namespace task.project.api.Domain.Entities
{
    public class SchedulerEntity : BaseEntity<int>
    {
        public string Name { get; set; }
        public string Position { get; set; }
        public IList<ScheduleEntity> Schedules { get; set; }
    }
}