using System;
using System.Collections.Generic;

namespace task.project.api.Domain.Entities
{
    public class RoleEntity : BaseEntity<Guid>
    {
        public string RoleName { get; set; }
        public IList<UserEntity> Users { get; set; }
    }
}