namespace task.project.api.Domain.Dto
{
    public class AccountDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}