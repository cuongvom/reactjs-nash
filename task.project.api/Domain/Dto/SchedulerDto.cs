namespace task.project.api.Domain.Dto
{
    public class SchedulerDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
    }
}