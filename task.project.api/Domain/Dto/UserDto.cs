using System;

namespace task.project.api.Domain.Dto
{
    public class UserDto
    {
        public string UserName { get; set; }
        public Guid Id { get; set; }
    }
}